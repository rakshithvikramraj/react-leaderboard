import React, { Component } from 'react';
import {BrowserRouter, Switch, Route} from "react-router-dom";
import Leaderboard from './components/Leaderboard';
import LeaderboardForm from './components/LeaderboardForm'


class App extends Component {
  render() {
    return (
       <BrowserRouter>
          <Switch>
            <Route path="/lb-top10" component={Leaderboard} exact/>
            <Route path="/lbform" component={LeaderboardForm} exact/>
          </Switch>
       </BrowserRouter>
    );
  }
}

export default App;
