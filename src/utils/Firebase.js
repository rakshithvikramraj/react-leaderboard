import * as firebase from 'firebase';

const config = {
    apiKey: "AIzaSyAtnQGeqrPLBqr6bBW4LiBBo3xj6JyxAQQ",
    authDomain: "leaderboard-b74e2.firebaseapp.com",
    databaseURL: "https://leaderboard-b74e2.firebaseio.com",
    projectId: "leaderboard-b74e2",
    storageBucket: "leaderboard-b74e2.appspot.com",
    messagingSenderId: "220082277830"
};
firebase.initializeApp(config);

const database = firebase.database();

export {firebase, database as default}
