import React, {Component} from 'react';
import LeaderboardItem from './LeaderboardItem';
import database from '../utils/Firebase';

export default class Leaderboard extends Component{
    state = {
       data:[]
    };
    componentDidMount() {
        const ScoreListRef = database.ref('points');
        ScoreListRef.on('value', (snapshot) =>{
            this.setState({
                data:[]
            });
            const Data = snapshot.val();
            Object.keys(Data).forEach((key)=>{
                this.setState(prevState=>({
                    data: [...prevState.data,Data[key]]
                }))

            });
        });

    };

    shouldComponentUpdate(nextProps, nextState, nextContext) {

        // console.log(this.state, nextState);

        return true;
    }

    render(){

        const Data = this.state.data
            .sort((a,b)=>b.score - a.score)
            .map((item,i)=>(
            <LeaderboardItem
                key={i}
                rank={i+1}
                college={item.college}
                points={item.score}
            />
        ));
        console.log(Data);




        return(<div>
                    <table margin="2" border="2">
                        <thead>
                        <tr>
                            <th>Rank</th>
                            <th>College</th>
                            <th>Points</th>
                        </tr>
                        </thead>
                        <tbody>
                        {Data}
                        </tbody>
                    </table>
               </div>
        );
    }
}