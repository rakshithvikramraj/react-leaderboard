import React, {Component} from 'react';
import database from '../utils/Firebase';

class LeaderboardForm extends Component{
    state={};

    handleCollegeIdChange = (e) => {
        const collegeID = e.target.value;
        this.setState({collegeID});
    };

    handleCollegeNameChange = (e) => {
        const collegeName = e.target.value;
        this.setState({collegeName});
    };

    handleCollegeScoreChange = (e) => {
        const collegeScore = e.target.value;
        this.setState({collegeScore});
    };

    handleSubmit = (e) =>{
      e.preventDefault();
      const collegeID = this.state.collegeID;
      const ScoreListRef = database.ref(`points/${collegeID}`);
      ScoreListRef.set({
          college:this.state.collegeName,
          score:this.state.collegeScore
      });

    };

    handleUpdateSubmit = (e) =>{
        e.preventDefault();
        const collegeID = this.state.collegeID;
        const ScoreListRef = database.ref(`points/${collegeID}/`);
        ScoreListRef.update({score:this.state.collegeScore});

    };

    render(){
        return(
            <div>
                <h1>Add new College</h1>
                <form onSubmit={this.handleSubmit}>
                    <label>College ID</label>&nbsp;
                    <input
                        type="text"
                        name="college_id"
                        onChange={this.handleCollegeIdChange}
                    />
                    <br/>
                    <label>College Name</label>&nbsp;
                    <input
                        type="text"
                        name="college_name"
                        onChange={this.handleCollegeNameChange}
                    />
                    <br/>
                    <label>College Score</label>&nbsp;
                    <input
                        type="text"
                        name="college_score"
                        onChange={this.handleCollegeScoreChange}
                    />
                    <br/>
                    <input
                        type="submit"
                        value="Add new college"
                    />
                </form>
                <hr/>
                <h1>Update Score</h1>
                <form onSubmit={this.handleUpdateSubmit}>
                    <label>College ID</label>&nbsp;
                    <input
                        type="text"
                        name="college_id"
                        onChange={this.handleCollegeIdChange}
                    />
                    <br/>
                    <label>College Score</label>&nbsp;
                    <input
                        type="text"
                        name="college_score"
                        onChange={this.handleCollegeScoreChange}
                    />
                    <br/>
                    <input
                        type="submit"
                        value="Update Score"
                    />
                </form>
                <hr/>
            </div>
        );
    }
}

export default LeaderboardForm;