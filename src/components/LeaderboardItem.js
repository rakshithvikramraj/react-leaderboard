import React from 'react';
import PropTypes from 'prop-types';

const LeaderboardItem = (
                {
                    rank,
                 college,
                 points
                }
               ) => (
                   <tr>
                       <td>{rank}</td>
                       <td>{college}</td>
                       <td>{points}</td>
                   </tr>
);

LeaderboardItem.propTypes = {
  rank:PropTypes.number,
  college:PropTypes.string,
  points:PropTypes.number
};

export default LeaderboardItem;